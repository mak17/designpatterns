package com.factory;

import com.factory.units.Factory;
import com.factory.units.Unit;
import com.factory.units.UnitFactory;
import com.factory.units.UnitType;

public class MainFactory {
    public static void runFactory() {
        System.out.println("------------------------------FACTORY START---------------------------");

        Factory factory = new UnitFactory();

        Unit tank = factory.createUnit(UnitType.TANK);
        Unit rifleman = factory.createUnit(UnitType.RIFLEMAN);

        System.out.println((tank.getHp() + " - " + tank.getExp() + " - " + tank.getDmgDone()));
        System.out.println((rifleman.getHp() + " - " + rifleman.getExp() + " - " + rifleman.getDmgDone()));

        System.out.println("------------------------------FACTORY END---------------------------");
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("/////////////////////////////////////////////////////////////////////////////////////////////////");
    }
}
