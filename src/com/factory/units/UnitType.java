package com.factory.units;

public enum UnitType {
    RIFLEMAN, TANK
}
