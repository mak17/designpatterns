package com.factory.units;

public class UnitFactory extends Factory {
    @Override
    public Unit createUnit(UnitType unitType) {
        switch (unitType) {
            case TANK:
                return  new Tank(100, 200,0);
            case RIFLEMAN:
                return new Rifleman(50, 100,0);
            default:
                throw new UnsupportedOperationException("No such type");
        }
    }
}
