package com;

import com.builder.MainBuilder;
import com.factory.MainFactory;
import com.flyweight.MainFlyweight;
import com.observer.main.MainObserver;
import com.singleton.MainSingleton;

public class Main {
    public static void main(String[] args) {
        /*
         * --------------------------DESIGN PATTERNS START--------------------------
         * What is it a design patterns?
         *   Design patterns are ready-made solutions for popular programming problems
         *
         * --------------------------DESIGN PATTERNS END----------------------------
         * */


        /*--------------------------------OBSERVER START--------------------------------*/
        MainObserver.runObserver();
        /*--------------------------------OBSERVER END----------------------------------*/


        /*--------------------------------SINGLETON START--------------------------------*/
        MainSingleton.runSingleton();
        /*--------------------------------SINGLETON END----------------------------------*/

        /*--------------------------------BUILDER START--------------------------------*/
        MainBuilder.runBuilder();
        /*--------------------------------BUILDER END----------------------------------*/

        /*--------------------------------BUILDER START--------------------------------*/
        MainFactory.runFactory();
        /*--------------------------------BUILDER END----------------------------------*/

        /*--------------------------------FLYWEIGHT START--------------------------------*/
        MainFlyweight.runFlyweight();
        /*--------------------------------FLYWEIGHT END----------------------------------*/
    }
}