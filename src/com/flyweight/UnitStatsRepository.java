package com.flyweight;

public class UnitStatsRepository {
    private static DestroyerUnitStats destroyerStats = new DestroyerUnitStats("Destroyer", 100, 100, 100, 100);
    private static RiflemanUnitStats riflemanStats = new RiflemanUnitStats("Rifleman", 200, 200, 200, 200);
    private static TesltankUnitStats teslaStats = new TesltankUnitStats("Teslatank", 300, 300, 300, 300);

    public static DestroyerUnitStats getDestroyer() {
        return destroyerStats;
    }

    public static RiflemanUnitStats getRiflemanStats() {
        return  riflemanStats;
    }

    public static TesltankUnitStats getTeslaStats() {
        return teslaStats;
    }
}
