package com.flyweight;

public class Tesltank {
    private int x;
    private int y;
    private TesltankUnitStats teslaTankStats;

    public Tesltank(int x, int y) {
        this.x = x;
        this.y = y;
        this.teslaTankStats = UnitStatsRepository.getTeslaStats();
    }
}
