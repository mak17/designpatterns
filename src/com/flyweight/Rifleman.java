package com.flyweight;

public class Rifleman {
    private int x;
    private int y;
    private RiflemanUnitStats riflemanUnitStats;

    public Rifleman(int x, int y) {
        this.x = x;
        this.y = y;
        this.riflemanUnitStats = UnitStatsRepository.getRiflemanStats();
    }
}
