package com.flyweight;

import java.util.ArrayList;
import java.util.List;

public class MainFlyweight {
    public static void runFlyweight() {

        List<Object> activeUnits = new ArrayList<>();

        for(int i = 0; i< 100000; i++) {
            activeUnits.add(new Tesltank(60,60));
            activeUnits.add(new Rifleman(40,40));
            activeUnits.add(new Destroyer(20,20));
        }
    }
}
