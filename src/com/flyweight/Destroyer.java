package com.flyweight;

public class Destroyer {
    private int x;
    private int y;
    private DestroyerUnitStats destroyerUnitStats;

    public Destroyer(int x, int y) {
        this.x = x;
        this.y = y;
        this.destroyerUnitStats = UnitStatsRepository.getDestroyer();
    }
}
