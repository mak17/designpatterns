package com.singleton;

import java.io.Serializable;

public class GuessGame implements Serializable {
    private static int score;
    private static GuessGame instance = new GuessGame();

    private GuessGame() {}

    public static int getScore() {
        return score;
    }
    public static GuessGame getInstance() {
        return instance;
    }

    public void play() {
        System.out.println("Run game!!!");
    }
    protected Object readResolve() {
        return getInstance();
    }
}
