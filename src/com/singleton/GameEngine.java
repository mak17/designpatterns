package com.singleton;

import java.io.Serializable;

public class GameEngine implements Serializable {
    private int hp = 100;
    private String name;
    /*Field instance and method getInstance have to are static because we have to trigger method without
     * createn object of GameEngine
     * */
    /*Solution 1*/
    /*private static GameEngine instance;*/
    /*Solution 2*/
    private static GameEngine instance = new GameEngine();

    private GameEngine() {

    }

    /*Method can be ansynchronized then two thread can get two different instances. If we want to avoid this
     * we can add key word 'synchronized' to the method. The minus is that only one thread can use this method so other
     * have to wait until first thread leave the method
     * */
    /*Solution 1*/
   /* public static synchronized GameEngine getInstance() {
        if (instance == null) {
            instance = new GameEngine();
        }

        return instance;
    }*/

    /*Second and better solution is create instance with variable declaration.
     * Then instance will be initiated when class will first trigger(This is will in class loader)
     * Then we don't need checking if instance exist or not, we only return instance, our method getInstance is easy get*/
    /*Solution 2*/
    public static GameEngine getInstance() {
        return instance;
    }

    public void run() {
        while (true) {
            //wait for input from user
            //change state of game
            //render graphics
        }
    }

    /*With Serializable
     * When class was serialized and we copy the result and unserialized then we have a few copies
     * If we want to avoid this we should use readResolve() method which protect us against it
     * */
    protected Object readResolve() {
        return getInstance();
    }
}
