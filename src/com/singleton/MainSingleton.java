package com.singleton;

public class MainSingleton {
    /*
     * Singleton ensure create only one instance of class
     * */
    public static void runSingleton() {
        GameEngine engine = GameEngine.getInstance();
        GameEngine engineTwo = GameEngine.getInstance();

        /*Here we should get true because of both references point to the same object*/
        System.out.println(engine == engineTwo);

        /*Test*/
        GuessGame game = GuessGame.getInstance();

        game.play();

        int score = game.getScore();

        GuessGame anotherGameReference = GuessGame.getInstance();

        if (game == anotherGameReference) {
            System.out.println("Singleton!");
            if (score == anotherGameReference.getScore()) {
                System.out.println("Scores are the same!");
            }
        }

        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("/////////////////////////////////////////////////////////////////////////////////////////////////");
    }

}
