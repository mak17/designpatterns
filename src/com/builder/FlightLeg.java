package com.builder;

import java.util.EmptyStackException;

public class FlightLeg {
    private String from;
    private String to;
    private boolean delayed;
    private Integer price;

    public FlightLeg(FlightLegBuilder flightLegBuilder) {
        this.from = flightLegBuilder.from;
        this.to = flightLegBuilder.to;
        this.delayed = flightLegBuilder.delayed;

        if(flightLegBuilder.price != null) {
            this.price = flightLegBuilder.price;
        } else {
            throw new IllegalStateException("Price is required");
        }
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public boolean getDelayed() {
        return delayed;
    }

    public Integer getPrice() {
        return price;
    }

    public void setDelayed(boolean delayed) {
        this.delayed = delayed;
    }

    @Override
    public String toString() {
        return "FlightLeg {" +
                "from = '" + from + '\'' +
                ", to = '" + to + '\'' +
                ", delayed = '" + delayed + '\'' +
                ", price = '" + price + '\'' +
                '}';
    }

    public static class FlightLegBuilder {
        private String from;
        private String to;
        private boolean delayed = false;
        private Integer price;

        public FlightLegBuilder(String from, String to) {
            this.from = from;
            this.to = to;
        }

        public FlightLeg build() {
            return new FlightLeg(this);
        }

        public FlightLegBuilder price(Integer price) {
            this.price = price;
            return this;
        }
    }
}
