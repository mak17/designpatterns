package com.builder;

public class MainBuilder {
    public static void runBuilder() {

        House house = new House.HouseBuilder()
                .buildWalls("walls")
                .buildFloors("floors")
                .buildRoof("roof")
                .buildDoors("doors")
                .build();

        System.out.println(house.toString());


        /*Test*/
        FlightLeg leg = new FlightLeg.FlightLegBuilder("Las Vegas", "Los Angeles").price(50).build();
        System.out.println(leg.toString());

        /*Without price*/
        //FlightLeg leg2 = new FlightLeg.FlightLegBuilder("Las Vegas","Los Angeles").build();
        //System.out.println(leg2.toString());

        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("/////////////////////////////////////////////////////////////////////////////////////////////////");
    }
}
