package com.observer.notification;

import com.observer.order.Order;

public class TextMessage implements Observer {
    public void updateOrderStatus(Order order) {
        String message = "TextMessage: order number: " + order.getOredrNumber() + " status: " + order.getStatus();
        System.out.println(message);
    }
}