package com.observer.notification;

import com.observer.weatherforecast.WeatherForecast;

public class RadioNews implements WeatherForecastObserver {
    @Override
    public void updateTemperature(WeatherForecast weatherForecast) {
        String message = "RadioNews - New temperature: " + weatherForecast.getTemperature();
        System.out.println(message);
    }

    @Override
    public void updatePressure(WeatherForecast weatherForecast) {
        String message = "RadioNews - New pressure: " + weatherForecast.getPressure();
        System.out.println(message);
    }
}
