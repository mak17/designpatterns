package com.observer.notification;

import com.observer.weatherforecast.WeatherForecast;

public interface WeatherForecastObserver {
    void updateTemperature(WeatherForecast weatherForecast);

    void updatePressure(WeatherForecast weatherForecast);
}