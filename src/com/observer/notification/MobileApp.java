package com.observer.notification;

import com.observer.order.Order;

public class MobileApp implements Observer {
    public void updateOrderStatus(Order order) {
        String message = "MobileApp: order number: " + order.getOredrNumber() + " status: " + order.getStatus();
        System.out.println(message);
    }
}