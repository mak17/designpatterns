package com.observer.notification;

import com.observer.order.Order;

public interface Observer {

    void updateOrderStatus(Order order);
}