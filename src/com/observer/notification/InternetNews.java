package com.observer.notification;

import com.observer.weatherforecast.WeatherForecast;

public class InternetNews implements WeatherForecastObserver {
    @Override
    public void updateTemperature(WeatherForecast weatherForecast) {
        String message = "InternetNews - New temperature: " + weatherForecast.getTemperature();
        System.out.println(message);
    }

    @Override
    public void updatePressure(WeatherForecast weatherForecast) {
        String message = "InternetNews - New pressure: " + weatherForecast.getPressure();
        System.out.println(message);
    }

}