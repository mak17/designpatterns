package com.observer.notification;

import com.observer.order.Order;

public class Email implements Observer {
    public void updateOrderStatus(Order order) {
        String message = "E-mail: order number: " + order.getOredrNumber() + " status: " + order.getStatus();
        System.out.println(message);
    }
}