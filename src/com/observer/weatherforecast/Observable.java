package com.observer.weatherforecast;

import com.observer.notification.WeatherForecastObserver;

public interface Observable {
    void registerObserver(WeatherForecastObserver observer);

    void unregisterObserver(WeatherForecastObserver observer);

    void notifyPressure();

    void notifyTemperature();
}