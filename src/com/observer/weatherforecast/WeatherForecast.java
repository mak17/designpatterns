package com.observer.weatherforecast;

import com.observer.notification.WeatherForecastObserver;

import java.util.HashSet;
import java.util.Set;

public class WeatherForecast implements Observable {
    private int temperature;
    private int pressure;
    private Set<WeatherForecastObserver> weatherForecastObservers = new HashSet<>();

    public WeatherForecast(int temperature, int pressure) {
        this.temperature = temperature;
        this.pressure = pressure;
    }

    public int getTemperature() {
        return temperature;
    }

    public int getPressure() {
        return pressure;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    @Override
    public void registerObserver(WeatherForecastObserver observer) {
        weatherForecastObservers.add(observer);
    }

    @Override
    public void unregisterObserver(WeatherForecastObserver observer) {
        weatherForecastObservers.remove(observer);
    }

    @Override
    public void notifyPressure() {
        for (WeatherForecastObserver observer : weatherForecastObservers) {
            observer.updatePressure(this);
        }
    }

    @Override
    public void notifyTemperature() {
        for (WeatherForecastObserver observer : weatherForecastObservers) {
            observer.updateTemperature(this);
        }
    }

    public void changeTemperature(int temperature) {
        setTemperature(temperature);
        this.notifyTemperature();
    }

    public void changePressure(int temperature) {
        setPressure(temperature);
        this.notifyPressure();
    }
}