package com.observer.order;

public enum Status {
    REGISTERED,
    SENT,
    DELIVERED
}