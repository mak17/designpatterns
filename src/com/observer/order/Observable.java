package com.observer.order;

import com.observer.notification.Observer;

public interface Observable {
    void registerObserver(Observer observer);

    void unregisterObserver(Observer observer);

    void notifyObserver();
}