package com.observer.order;

import com.observer.notification.Observer;

import java.util.HashSet;
import java.util.Set;

public class Order implements Observable {
    private int oredrNumber;
    private Status status;
    private Set<Observer> registeredObservers = new HashSet<>();

    public Order(int oredrNumber, Status status) {
        this.oredrNumber = oredrNumber;
        this.status = status;
    }

    public void setOredrNumber(int oredrNumber) {
        this.oredrNumber = oredrNumber;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getOredrNumber() {
        return oredrNumber;
    }

    public Status getStatus() {
        return status;
    }

    public void changeStatus(Status status) {
        setStatus(status);
        notifyObserver();
    }

    @Override
    public void registerObserver(Observer observer) {
        registeredObservers.add(observer);
    }

    @Override
    public void unregisterObserver(Observer observer) {
        registeredObservers.remove(observer);
    }

    @Override
    public void notifyObserver() {
        for (Observer observer : registeredObservers) {
            observer.updateOrderStatus(this);
        }
    }
}