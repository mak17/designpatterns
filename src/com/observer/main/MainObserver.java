package com.observer.main;

import com.observer.notification.*;
import com.observer.order.Order;
import com.observer.order.Status;
import com.observer.weatherforecast.WeatherForecast;

public class MainObserver {
    /*
     * Notifications about change state of variable/objects to other state variable/objects
     *
     * 1) Create the observer interface with notify method.
     *       This method will be notification when something was change
     * 2) Create the channels of notify: Tv News, Text Message.
     *       Implement interface and overwrite the method
     * 3) Create the class with some variables which can be change
     * 4) Add the variable set of notifications for the class.
     *       This variable contains notifications which should be trigger
     * 5) Add methods:
     *       a) registerObserver -> add notification to the list
     *       b) unregisterObserver -> remove notification from the list
     *       c) notify -> go through the list of notifications and trigger method responsibility for
     *           notify the user/object about change
     * 6) Add method to the class which can made change on some field.
     *       After change we should trigger the notify method
     * 7) Run observer:
     *       a) Create the object of class ( Order. WeatherForecast)
     *       b) Create instances of notifications
     *       c) Register notifications
     *       d) Make a change
     * */

    public static void runObserver() {
        Order order = new Order(110, Status.REGISTERED);

        Email email = new Email();
        MobileApp mobileApp = new MobileApp();
        TextMessage textMessage = new TextMessage();

        order.registerObserver(email);
        order.registerObserver(mobileApp);
        order.registerObserver(textMessage);

        order.notifyObserver();
        System.out.println("------------------------------");

        order.changeStatus(Status.SENT);
        System.out.println("------------------------------");

        order.unregisterObserver(textMessage);
        order.changeStatus(Status.DELIVERED);
        System.out.println("------------------------------");

        WeatherForecast weatherForecast = new WeatherForecast(22, 1000);
        WeatherForecast weatherForecast_2 = new WeatherForecast(33, 500);

        RadioNews radioNews = new RadioNews();
        InternetNews internetNews = new InternetNews();
        TvNews tvNews = new TvNews();

        weatherForecast.registerObserver(radioNews);
        weatherForecast_2.registerObserver(internetNews);
        weatherForecast_2.registerObserver(tvNews);

        System.out.println("-------------weatherForecast-----------------");
        weatherForecast.notifyPressure();
        weatherForecast.notifyTemperature();
        System.out.println("-------------weatherForecast_2-----------------");
        weatherForecast_2.notifyPressure();
        weatherForecast_2.notifyTemperature();

        System.out.println("---------------changeTemperature weatherForecast---------------");
        weatherForecast.changeTemperature(10);
        weatherForecast.changePressure(4000);

        System.out.println("---------------changeTemperature weatherForecast_2---------------");
        weatherForecast_2.changeTemperature(100);
        weatherForecast_2.changePressure(222);

        System.out.println("#################################################");

        weatherForecast_2.unregisterObserver(internetNews);
        System.out.println("---------------changeTemperature weatherForecast---------------");
        weatherForecast.changeTemperature(103);
        weatherForecast.changePressure(222);

        System.out.println("---------------changeTemperature weatherForecast_2---------------");
        weatherForecast_2.changeTemperature(-22);
        weatherForecast_2.changePressure(11111);

        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("/////////////////////////////////////////////////////////////////////////////////////////////////");
    }
}
